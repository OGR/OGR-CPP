#include "Waypoint.hpp"
#include <cstring>

using namespace std;

Waypoint::Waypoint(int t_number, string t_name, double t_latitude, double t_longitude){
	number = t_number;
	name = t_name;
	latitude = t_latitude;
	longitude = t_longitude;
};
