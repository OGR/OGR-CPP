#include <string>

using namespace std;

class Waypoint{
	public:
		// Constructeur
		Waypoint(int t_number, string t_name, double t_latitude, double t_longitude);

		// Attributes
		int number;
		string name;
		double latitude;
		double longitude;
};
