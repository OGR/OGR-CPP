#include <iostream>
#include <iomanip>
#include <fstream>
#include <string>
#include <regex>
#include <exception>
#include <vector>

#include "Waypoint.hpp"

using namespace std;

int main(int argc, char* argv[])
{

	ifstream file_cvt;
	ofstream file_gpx;

	file_cvt.exceptions (ifstream::failbit | ifstream::badbit | ifstream::eofbit);

	string line_cvt, line_wpt1, line_wpt2;
	string route_name;

	string gpx_header {
		R"(<?xml version="1.0"?>)" "\n"
		R"(<gpx version="1.1")" "\n"
		R"(	xmlns="http://www.topographix.com/GPX/1/1")" "\n"
		R"(	xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance")" "\n"
		R"(	xmlns:xsd="http://www.x3.org/2001/XMLSchema">)" "\n"
	};
	string gpx_footer {R"(</gpx>)"};
	string gpx_route_begin {"	<rte>"};
	string gpx_route_end {"	</rte>"};

	string wpt_name;
	int wpt_number;
	double lat1, lat2, lon1, lon2;

	vector<Waypoint> waypoints;
	vector<Waypoint>::iterator ite;

	const regex pattern_route_name {"^;Route (.+)"};
	const regex pattern_wpt_number_name {"^WP (\\d+) (\\w+)"};
	const regex pattern_wpt_coordinates {"Lat\\s+(\\d{1,2})\\D+(\\d{2}\\.\\d{6})([NS])\\s+Lon\\s+(\\d{1,3})\\D+(\\d{2}\\.\\d{6})([WE])"}; // Maybe I can use only one pattern two times.
	smatch match_cvt;

	cout << "OGR : Outil de Gestion de Routes" << endl;
	cout << "--------------------------------" << endl;

	// Opening cvt file.
	try{
		file_cvt.open(argv[1]);
		cout << "Fichier ouvert : " << argv[1] << endl;
		// Reading the name of the route from the cvt file.
		getline(file_cvt,line_cvt);
		regex_search(line_cvt, match_cvt, pattern_route_name);
		route_name = match_cvt.str(1);
		cout << "Nom de la route : " << match_cvt.str(1) << endl;
		cout << "--------------------------------" << endl;
	}
	catch(ifstream::failure e){
		cerr << "Error while attempting to open file" << endl;
	}

	while(file_cvt.is_open()){
		try{
			getline(file_cvt, line_cvt);
			if (line_cvt.compare(0,1,";") == 0){
				/* line_cvt is two characters long (\n ?),
			 	* so we should compare only one of them,
			 	* from the first position (i.e. 0) */
				//cout << "Ligne suivante : ok" << endl;
				getline(file_cvt, line_wpt1);
				getline(file_cvt, line_wpt2);
				//cout << "Ligne 1 : " << line_wpt1 << endl;
				//cout << "Ligne 2 : " << line_wpt2 << endl;
				//cout << "--------------" << endl;

				//waypoints.push_back(Waypoint(*pline_wpt1, *pline_wpt2);
				// Parse wpt - numero & name
				regex_search(line_wpt1, match_cvt, pattern_wpt_number_name);
				//cout << "Numéro du waypoint : " << match_cvt.str(1) << endl;
				//cout << "Nom du waypoint : " << match_cvt.str(2) << endl;
				wpt_number = stoi(match_cvt.str(1));
				wpt_name = match_cvt.str(2);

				regex_search(line_wpt2, match_cvt, pattern_wpt_coordinates);
				// Uncomment to verify that it matches (verify regex !)
				//cout << "Match : " << regex_search(line_wpt2, match_cvt, pattern_wpt_coordinates) << endl;
				//cout << "--------------" << endl;

				// Parse wpt - coordinates
				// Parse wpt - latitude
				//cout << "Latitude : " << match_cvt.str(1) << "°" << match_cvt.str(2) << " " << match_cvt.str(3) << endl;
				lat1 = stod(match_cvt.str(1));
				lat2 = stod(match_cvt.str(2));
				//cout << "Integer part of latitude : " << lat1 << endl;
				//cout << fixed << setprecision(6) << "Decimal part of latitude : " << lat2 << endl;
				lat2 = lat2/60;
				lat1 = lat1 + lat2;
				//cout << "Converted decimal part of latitude : " << lat2 << endl;
				if(!match_cvt.str(3).compare("S")) lat1 = -lat1;
				//cout << "Final latitude : " << lat1 << endl;
				//cout << "--------------" << endl;

				// Parse wpt - longitude
				//cout << "Longitude : " << match_cvt.str(4) << "°" << match_cvt.str(5) << " " << match_cvt.str(6) << endl;
				lon1 = stod(match_cvt.str(4));
				lon2 = stod(match_cvt.str(5));
				//cout << "Integer part of longitude : " << lon1 << endl;
				//cout << fixed << setprecision(6) << "Decimal part of longitude : " << lon2 << endl;
				lon2 = lon2/60;
				lon1 = lon1 + lon2;
				//cout << "Converted decimal part of longitude : " << lon2 << endl;
				if((!match_cvt.str(6).compare("W"))) lon1 = -lon1;
				//cout << "Final longitude : " << lon1 << endl;
				//cout << "--------------------------------" << endl;
				//cout << "--------------------------------" << endl;

				waypoints.push_back(Waypoint(wpt_number, wpt_name, lat1, lon1));
			}
		}
		catch(ifstream::failure e){
			//cerr << "Fichier parsé" << endl;
			cout << "Fichier parsé" << endl;
			cout << waypoints.size() << " waypoints trouvés" << endl;
			cout << "--------------------------------" << endl;
			file_cvt.close();
		}
	}
	// Show all waypoints using an iterator.
	for(ite=waypoints.begin(); ite!=waypoints.end(); ite++){
		cout << endl;
		cout << "Waypoint number : " << ite->number << endl;
		cout << "Waypoint name : " << ite->name << endl;
		cout << fixed << setprecision(6) << "Waypoint latitude: " << ite->latitude << endl;
		cout << fixed << setprecision(6) << "Waypoint longitude : " << ite->longitude << endl;
		/* Should use '*ite' to access the Waypoint object.
		 * '*ite.number' won't work as the '.' opereator works
		 * before the '*' operator. '(*ite).number' works.
		 * 'ite->number' is equivalent. */
	}	

	// Export to a GPX file.
	file_gpx.open(route_name+".gpx");
	file_gpx << fixed << setprecision(6);
	file_gpx << gpx_header;
	file_gpx << gpx_route_begin << endl;
	file_gpx << R"(	<name>)" << route_name << R"(</name>)" << endl;
	for(ite=waypoints.begin(); ite!=waypoints.end(); ite++){
		file_gpx << R"(		<rtept)";
		file_gpx << R"( lat=")" << ite->latitude << R"(")";
		file_gpx << R"( lon=")" << ite->longitude << R"(")";
		file_gpx << R"(>)" << endl;
		file_gpx << R"(			<name>)" << ite->name << R"(</name>)" << endl;
		file_gpx << R"(		</rtept>)" << endl;
	}
	file_gpx << gpx_route_end << endl;
	file_gpx << gpx_footer;
	file_gpx.close();

	cout << endl;
	return 0;
}
